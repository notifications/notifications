package name.li.notificationservice.server.impl;

import name.li.notificationservice.Server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NotificationSender implements Server {
	
	private Logger log = LoggerFactory.getLogger(this.getClass()); 

	@Override
	public void sendNotification(String user) {
		System.out.println(user);
		log.debug("Notification to {}", user);
	}

}
