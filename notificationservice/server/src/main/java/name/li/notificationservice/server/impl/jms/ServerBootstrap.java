package name.li.notificationservice.server.impl.jms;

import java.util.concurrent.CountDownLatch;

import name.li.notificationservice.server.impl.NotificationSender;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.spi.Filter;
import org.apache.log4j.spi.LoggingEvent;

import com.google.common.eventbus.AllowConcurrentEvents;

public class ServerBootstrap {
	public static void main(String[] args) throws Exception {
		
		ConsoleAppender console = new ConsoleAppender(); //create appender
		  //configure the appender
		  String PATTERN = "%d [%p|%c|%C{1}] %m%n";
		  console.setLayout(new PatternLayout(PATTERN)); 
		  console.setThreshold(Level.DEBUG);
		  console.activateOptions();
		  console.addFilter(new Filter() {
			
			@Override
			public int decide(LoggingEvent event) {
//				return ACCEPT;
				return !event.getLocationInformation().getClassName().contains("kahadb") ? ACCEPT : DENY;
			}
		});
		  //add appender to any Logger (here is root)
		  Logger.getRootLogger().removeAllAppenders();
		  Logger.getRootLogger().addAppender(console);
		  
		  
		  
		BrokerService broker = new BrokerService();
		CountDownLatch latch = new CountDownLatch(1);
		broker.addConnector("tcp://localhost:61616");
		broker.start();
		
		
		ActiveMQConnection connection = ActiveMQConnection.makeConnection("tcp://localhost:61616");
		JmsTransport transport = new JmsTransport("sample", new NotificationSender(), connection);
		transport.connect();
		
		
//		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
//		
//		
//		Topic topic = session.createTopic("Foo!");
//		MessageProducer producer = session.createProducer(topic);
//		MessageConsumer consumer = session.createConsumer(topic);
//		
//		new NotificationSender().sendNotification("John");
	}
}
