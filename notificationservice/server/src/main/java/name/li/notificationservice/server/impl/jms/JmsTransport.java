package name.li.notificationservice.server.impl.jms;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import name.li.notificationservice.server.impl.NotificationSender;

public class JmsTransport {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	private static long VERSION = -1L;

	private NotificationSender sender;
	private Connection connection;
	private boolean stopped;
	private String clientId;

	public JmsTransport(String clientId, NotificationSender sender, Connection connection) {
		this.sender = sender;
		this.connection = connection;
		this.clientId = clientId;
	}

	public void connect() {
		assertState();
		try {
			connection.start();
			attach();
		} catch (Exception e) {
			try {
				connection.close();
			} catch (JMSException e1) {
				log.error("Got exception {}", e1);
			}
			throw new RuntimeException(e);
		}

	}

	public void disconnect() {
		assertState();
		try {
			connection.stop();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	private void assertState() {
		if (stopped)
			throw new RuntimeException("Server is stopped already");
	}

	private void attach() throws JMSException {
		handleUtility();
		handleMessageSending();
	}

	private void handleMessageSending() throws JMSException {
		Session messageSendingSession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Topic topic = messageSendingSession.createTopic("notificationservice");
		MessageConsumer consumer = messageSendingSession.createConsumer(topic);
		consumer.setMessageListener(message -> {
			log.debug("normal handler");
			try {
				String destination = message.getStringProperty("destination");
				sender.sendNotification(destination);
				log.debug("Server got message {}", message);
				Destination replyDestination = message.getJMSReplyTo();
				if (replyDestination != null) {
					log.debug("Replying to: {}", replyDestination);
					messageSendingSession
						.createProducer(replyDestination)
						.send(messageSendingSession.createTextMessage("OK!"));
				}
			} catch (Exception e) {
				log.error("Something went wrong {}", e);
				handlerErrorInListener(message, e);
			}
		});
		log.debug("normal handler added: {}", consumer);
	}

	private void handleUtility() throws JMSException {
		Session utilitySession = connection.createSession(false, Session.DUPS_OK_ACKNOWLEDGE);
		Topic topic = utilitySession.createTopic("notificationservice");

		MessageConsumer protocolVersionMessageConsumer = utilitySession.createConsumer(topic, "clientId = " + clientId);
		protocolVersionMessageConsumer.setMessageListener(message -> {
			try {
				log.debug("Version handler");
				log.debug("Got version verification message: {}", message);
				Message replyMessage = isMessageVersionSupported(message)
						? createVersionOkMessage(utilitySession)
						: createVersionWrongMessage(utilitySession, String.format("Version {0} not supported", VERSION));
				MessageProducer producer = null;
				try {
					log.debug("Replying to version verification message...");
					MessageProducer replyProducer = utilitySession.createProducer(message.getJMSReplyTo());
					replyProducer.send(replyMessage);
					log.debug("Reply is sent.");
				} finally {
					if (producer != null)
						producer.close();
				}
			} catch (Exception e) {
				handlerErrorInListener(message, e);
			}
		});
		log.debug("Version handler added: {}", protocolVersionMessageConsumer);
	}

	private Message createVersionOkMessage(Session session) throws JMSException {
		Message message = session.createMessage();
		message.setBooleanProperty("supports", true);
		return message;
	}

	private Message createVersionWrongMessage(Session session, String reason) throws JMSException {
		Message message = session.createMessage();
		message.setBooleanProperty("supports", false);
		message.setStringProperty("cmoplain", reason);
		return message;
	}

	private boolean isMessageVersionSupported(Message message) throws JMSException {
		return message.getLongProperty("version") == VERSION;
	}

	private void handlerErrorInListener(Message failedMessage, Exception e) {
	}

}
