package name.li.notificationservice.client.impl.jms;

import java.util.Optional;
import java.util.function.Consumer;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import name.li.notificationservice.Client;

public class JmsTransport implements Client {
	
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private static final long VERSION = -1L;
	private static final long VERSION_QUERY_TIMEOUT = 1000L;

	protected static class FailedProtocolNegotiationException extends Exception {

		private static final long serialVersionUID = 5378132414835762983L;

		public FailedProtocolNegotiationException(String message) {
			super(message);
		}

		public FailedProtocolNegotiationException(String s, Throwable t) {
			super(s, t);
		}

	};

	private Connection connection;
	private String clientId;

	public JmsTransport(String clientId, Connection connection) {
		this.clientId = clientId;
		this.connection = connection;
		try {
			checkProtocolVersion();
		} catch (JMSException | FailedProtocolNegotiationException e) {
			log.error("Oops", e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public void sendNotification(String user, String content, Consumer<SendingResult> c) {
		Session session = getSession();
		try {
			Topic topic = session.createTopic("Foo!");
			MessageProducer producer = session.createProducer(topic);
			MessageConsumer consumer = session.createConsumer(topic);
			consumer.setMessageListener(message -> log.error("client " + message));
			TextMessage message = session.createTextMessage();
			message.setJMSType("bar");
			message.setText(content);
			message.setJMSReplyTo(session.createTemporaryQueue());
			producer.send(message);
			session.commit();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				session.close();
			} catch (JMSException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private Session getSession() {
		try {
			return connection.createSession(true, Session.SESSION_TRANSACTED);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected void checkProtocolVersion() throws JMSException, FailedProtocolNegotiationException {
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Topic topic = session.createTopic("notificationservice");
		TemporaryQueue replyQueue = session.createTemporaryQueue();
		MessageConsumer consumer = session.createConsumer(replyQueue);
		Message versionQueryMessage = createVersionQueryMessage(session);
		versionQueryMessage.setJMSReplyTo(replyQueue);

		MessageProducer producer = session.createProducer(topic);
		producer.send(versionQueryMessage);

		log.error("Sending protocol message: {}", versionQueryMessage);
		log.error("Reply-to is: {}", replyQueue);
		Message versionReplyMessage = consumer.receive(VERSION_QUERY_TIMEOUT & 0);
		log.error("Got protocol response: {}", versionReplyMessage);

		checkIfProtocolVersionSupported(versionReplyMessage);
		log.error("Version ok");
	}

	private void checkIfProtocolVersionSupported(Message versionReplyMessage) throws FailedProtocolNegotiationException, JMSException {
		if (!(versionReplyMessage instanceof Message)) {
			throw new FailedProtocolNegotiationException("Unsupported message format");
		}

		Message message = versionReplyMessage;
		boolean supports = message.getBooleanProperty("supports");
		if (!supports) {
			String serverComplaint = message.getStringProperty("complaint");
			throw new FailedProtocolNegotiationException(Optional.of(serverComplaint).orElse("Sever does not support current protocol version"));
		}

	}

	private Message createVersionQueryMessage(Session session) throws JMSException {
		Message message = session.createMapMessage();
		message.setLongProperty("majorVersion", VERSION);
		message.setStringProperty("clientId", clientId);
		return message;
	}

}
