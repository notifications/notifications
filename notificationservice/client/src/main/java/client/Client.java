package client;

import java.net.URISyntaxException;

import javax.jms.JMSException;

import name.li.notificationservice.client.impl.jms.JmsTransport;

import org.apache.activemq.ActiveMQConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Client {
	public static void main(String[] args) throws JMSException, URISyntaxException {
		ActiveMQConnection connection = ActiveMQConnection.makeConnection("tcp://localhost:61616");
		new JmsTransport("sample", connection).sendNotification("John", "boo!");
	}
}
