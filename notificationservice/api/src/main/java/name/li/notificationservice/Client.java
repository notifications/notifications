package name.li.notificationservice;

import java.util.function.Consumer;

public interface Client {

	default void sendNotification(String user, String content) {
		sendNotification(user, content, sendingResult -> {});
	}

	void sendNotification(String user, String content, Consumer<SendingResult> c);

	class SendingResult {
		enum Status {
			OK, FAILED
		}

		private Status status;

		public SendingResult(Status status) {
			this.status = status;
		}

		public Status getStatus() {
			return status;
		}

	}

}
